#!/bin/bash
set -exuo pipefail

apt-get update
apt-get install -y git build-essential curl python3

git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git
export PATH=$(pwd)/depot_tools:$PATH

git clone https://github.com/SignalApp/ringrtc.git
cd ringrtc
make electron PLATFORM=unix